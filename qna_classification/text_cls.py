import sklearn
import sklearn.datasets
import pandas as pd
import pymysql
import time
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import seaborn as sns
import requests


from sklearn.feature_extraction.text import CountVectorizer
from sklearn.preprocessing import normalize
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE

from bs4 import BeautifulSoup
import re
from konlpy.tag import Twitter
from scipy.sparse import csr_matrix
import numpy as np


def get_db(sql):
    info = pymysql.connect(
        user='bigjh64',
        passwd='bigjh64$31',
        host='db-1ea6m.pub-cdb.ntruss.com', # 운영db
        # host='db-svmk.pub-cdb.ntruss.com' # dev db
        port=3306,
        db='cwaidata',
        charset='utf8'
    )

    cursor = info.cursor(pymysql.cursors.DictCursor)
    cursor.execute(sql)
    reuslt=cursor.fetchall()

    df = pd.DataFrame(reuslt)
    # df.to_csv('./table/prj_2391_hist.csv', index=False)

    return df

def preprocessing(sentence):
    soup = BeautifulSoup(sentence, "html5lib")
    remove_tag = soup.get_text()
    sentence = re.sub('[-=+,#/\?:^$.@*\"※~&%ㆍ!』\\‘|\(\)\[\]\<\>`\'…》;]', '', remove_tag)
    # sentence =re.sub('[^가-힣ㄱ-ㅎㅏ-ㅣa-zA-Z]', ' ', sentence, flags=re.MULTILINE)

    return sentence



def tw_tokenizer(text):
    twitter = Twitter()
    # 입력 인자로 들어온 text 를 형태소 단어로 토큰화 하여 list 객체 반환
    tokens_ko = twitter.nouns(text)
    return tokens_ko



def text_clustering(text_series, k_num):
    # Token
    vectorizer = CountVectorizer()
    X = vectorizer.fit_transform(text_series)
    # print('Toknend shape ', X.shape)

    # Normalization
    X = normalize(X)
    npX = X.toarray()

    # K-means training
    print("Star training...")
    kmeans = KMeans(n_clusters=k_num, random_state=111).fit(X)
    print("\nFinished")

    # Result
    labels = kmeans.labels_
    # centers = kmeans.cluster_centers_


    # Dim reduction
    Pemb = PCA(n_components=5).fit_transform(npX) # PCA
    Temb = TSNE(n_components=2).fit_transform(Pemb) # TSNE
    # print("PCA embedded : {}\nTSNE embedded : {}".format(Pemb.shape, Temb.shape))
    emb_df = pd.DataFrame()
    emb_df['x'] = Temb[:, 0]
    emb_df['y'] = Temb[:, 1]
    emb_df['label'] = labels

    return labels, emb_df


def qna_clustering(sdate, edate):
    """
    question_type_cd 종류
        ETC: 기타
        MEM_REG: 회원가입
        POINT: 포인트
        PROJECT: 프로젝트
        USE_SITE: 사이트 이용방법
    """

    # string to datetime
    start_date = datetime.strptime(sdate, '%Y-%m-%d')
    end_date = datetime.strptime(edate, '%Y-%m-%d')

    # Get [CW_QNA] Table
    sql = "SELECT qna_id, question_type_cd, project_id, question_title, question, open_yn, answer_yn,  answer, answer_date, reg_date, reg_user\
        FROM `CW_QNA` WHERE `reg_date` >= '{}' AND `reg_date` < '{}';".format(start_date, end_date)

    df = get_db(sql)

    # Trainset
    df = df[['qna_id', 'project_id', 'question_type_cd', 'question', 'reg_date']]

    # Text Clear
    df['question_cleaned'] = df['question'].apply(preprocessing)
    df = df[df['question_type_cd'] == 'PROJECT'].reset_index(drop=True) # Only Project QnA
    # df = df[df['project_id'] == 2576].reset_index(drop=True) # Project Set

    print("QnA table shape = ", df.shape)
    question = df['question_cleaned'].tolist()

    # for q in question:
    #     res = requests.get("http://192.168.51.229:8088/keywords/analyzer?query={}".format(q))
    #     print(res)

    # Train K-means Clustering
    k_num = 3
    labels, emb_df = text_clustering(question, k_num)
    df['label'] = labels # add my df

    # Sort & table save 
    df = df[['qna_id', 'project_id', 'question_cleaned', 'reg_date', 'label']]

    # Sort maximum count label
    save_df = pd.DataFrame(columns=['qna_id', 'project_id', 'question_cleaned', 'reg_date', 'label'])
    for d in df['label'].value_counts().index:
        save_df = save_df.append(df[df['label'] == d].reset_index(drop=True), ignore_index=True)

    for k in range(k_num):
        tmp_df = save_df[save_df['label'] == k].reset_index(drop=True)
        print("Label Number = ", k)

        # Words counting
        tok_dict = {}
        for q in tmp_df['question_cleaned']:
            tok = tw_tokenizer(q)
            for t in tok:
                if t in tok_dict.keys():
                    tok_dict[t] += 1
                else:
                    tok_dict[t] = 1
        
        # Sort by value count
        tok_dict = sorted(tok_dict.items(), reverse=True, key=lambda item: item[1])
        print("\nTop 10 Words")
        for i, (key, value) in enumerate(tok_dict):
            print(key, value)
            if (i+1) == 10:
                break

    # Save
    save_df.to_csv('./qna_result.csv')
    print(df['label'].value_counts())

    # Text Visualization
    sns.scatterplot(x='x', y='y', hue="label", style="label", data=emb_df, palette="Set2")
    plt.show()
        
    


def reject_reason_clustering(sdate, edate):
    print("Start Date ~ End Date ==> {} ~ {}".format(sdate, edate))
    start_date = datetime.strptime(sdate, '%Y-%m-%d')
    end_date = datetime.strptime(edate, '%Y-%m-%d')

    # Get [CW_QNA] Table
    sql = "SELECT reject_id, reject_date, reject_reason, data_id, project_id, worker_id, checker_id \
        FROM `CW_REJECT` WHERE reject_date >= '{}' AND reject_date < '{}';".format(start_date, end_date)

    # Get table
    df = get_db(sql)
    print("Table shape : ", df.shape)

    # Clear text
    df['reject_reason_cleaned'] = df['reject_reason'].apply(preprocessing)    

    # Train Clustering
    k_num = 8
    labels, emb_df = text_clustering(df['reject_reason_cleaned'].to_list(), k_num)
    df['label'] = labels

    # Maximum count sorting
    save_df = pd.DataFrame(columns=['reject_reason_cleaned', 'label'])
    for d in df['label'].value_counts().index:
        save_df = save_df.append(df[['reject_reason_cleaned', 'label']][df['label'] == d].reset_index(drop=True), ignore_index=True)

    # Save text table
    save_df.to_csv('./reject_reason_clustered.csv', index=False)

    # Embedded Text Visualization
    sns.scatterplot(x='x', y='y', hue="label", style="label", data=emb_df, palette="Set2")
    plt.show()
            

if __name__ == '__main__':
    qna_clustering('2020-05-26', '2020-05-27')
    # reject_reason_clustering('2020-05-07', '2020-05-08')