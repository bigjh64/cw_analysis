import sklearn
import sklearn.datasets
import pandas as pd
import pymysql
import time
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import seaborn as sns
import requests


from sklearn.feature_extraction.text import CountVectorizer
from sklearn.preprocessing import normalize
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE

from bs4 import BeautifulSoup
import re
from konlpy.tag import Twitter
from scipy.sparse import csr_matrix
import numpy as np

def preprocessing(sentence):
    soup = BeautifulSoup(sentence, "html5lib")
    remove_tag = soup.get_text()
    sentence = re.sub('[-=+,#/\?:^$.@*\"※~&%ㆍ!』\\‘|\(\)\[\]\<\>`\'…》;]', '', remove_tag)
    # sentence =re.sub('[^가-힣ㄱ-ㅎㅏ-ㅣa-zA-Z]', ' ', sentence, flags=re.MULTILINE)

    return sentence



def tw_tokenizer(text):
    twitter = Twitter()
    # 입력 인자로 들어온 text 를 형태소 단어로 토큰화 하여 list 객체 반환
    tokens_ko = twitter.nouns(text)
    return tokens_ko



def text_clustering(text_series, k_num):
    # Token
    vectorizer = CountVectorizer()
    X = vectorizer.fit_transform(text_series)
    # print('Toknend shape ', X.shape)

    # Normalization
    X = normalize(X)
    npX = X.toarray()

    # K-means training
    print("Star training...")
    kmeans = KMeans(n_clusters=k_num, random_state=111).fit(X)
    print("\nFinished")

    # Result
    labels = kmeans.labels_
    # centers = kmeans.cluster_centers_


    # Dim reduction
    Pemb = PCA(n_components=5).fit_transform(npX) # PCA
    Temb = TSNE(n_components=2).fit_transform(Pemb) # TSNE
    # print("PCA embedded : {}\nTSNE embedded : {}".format(Pemb.shape, Temb.shape))
    emb_df = pd.DataFrame()
    emb_df['x'] = Temb[:, 0]
    emb_df['y'] = Temb[:, 1]
    emb_df['label'] = labels

    return labels, emb_df


def main():
    df = pd.read_csv('./table/CW_PROJECT.csv')

    #################### Preprocessing
    df = df[['project_id', 'project_progress_cd', 'reg_date', 'end_date', 'project_name', 'project_desc']]

    # to datetime
    for d in ['reg_date', 'end_date']:
        df[d] = pd.to_datetime(df[d])

    df = df[df['reg_date'] >= '2019-04-01 00:00:00'].reset_index(drop=True)
    df = df[df['end_date'] < '2020-04-01 00:00:00'].reset_index(drop=True)

    df = df[df['project_progress_cd'] == 'ENDED'].reset_index(drop=True)

    # project_name + project_desc
    df = df.fillna('')
    df['prj_name_desc'] = df['project_name'] + df['project_desc']

    df['desc_cleaned'] = df['project_name'].apply(preprocessing)
    
    # delete columns
    df = df.drop(['project_name', 'project_desc', 'reg_date', 'end_date', 'prj_name_desc'], axis=1)

    # Text clustering & Dim reduction
    labels, emb_df = text_clustering(df['desc_cleaned'], 5)
    df['label'] = labels

    sort_df = pd.DataFrame(columns=df.keys().to_list())
    for d in df['label'].value_counts().index:
        sort_df = sort_df.append(df[df['label'] == d].reset_index(drop=True), ignore_index=True)

    print(sort_df)
    sort_df.to_csv("project_attr_clustered.csv", index=False)
    sns.scatterplot(x='x', y='y', hue='label', data=emb_df, palette='Set2')
    plt.show()

    


if __name__ == '__main__':
    main()