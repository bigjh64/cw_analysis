import pandas as pd
# import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import os
import time
import json
from tqdm import tqdm

def bbox_iou(boxA, boxB):
    # determine the (x, y)-coordinates of the intersection rectangle
    xA = max(boxA[0], boxB[0])
    yA = max(boxA[1], boxB[1])
    xB = min(boxA[2], boxB[2])
    yB = min(boxA[3], boxB[3])

    # compute the area of intersection rectangle
    interArea = abs(max((xB - xA, 0)) * max((yB - yA), 0))
    if interArea == 0:
        return 0

    # abs area
    boxAArea = abs((boxA[2] - boxA[0]) * (boxA[3] - boxA[1]))
    boxBArea = abs((boxB[2] - boxB[0]) * (boxB[3] - boxB[1]))

    # compute the intersection over union by taking the intersection
    iou = interArea / float(boxAArea + boxBArea - interArea)

    return iou


def sequence_bboxes(input_df):
    seq_json = []
    for i in input_df['result_json']:
        
        if type(i) == str:
            rj = json.loads(i) # result_json
            curr_json = []
            
            print(rj['p_01']['box'])
            print("=")
            print(rj['p_01']['data'])

    
            for j in rj['data']: # Number of Object
                print(j)
                print()
                
                label = j['data'][0]['value']

                for b in j['dots']: # Get bbox
                    if b['idx'] == 0: # TL
                        x1, y1 = b['x'], b['y']

                    elif b['idx'] == 2: # BR
                        x2, y2 = b['x'], b['y']

                bbox = [x1, y1, x2, y2, label]
                curr_json.append(bbox)

            seq_json.append(curr_json) # result json sequence information
        else:
            print("error data id\n", input_df['data_id'])
            print()
            print("error result_json\n", input_df['result_json'])

    return seq_json
    

def main():
    tb_path = './table/prj_pig_hist.csv'
    df = pd.read_csv(tb_path)

    data_dup = df.groupby(['data_id']).size().reset_index()
    data_dup = data_dup.rename(columns = {0 : 'rework'})

    # Get Reject Data
    reject_data = data_dup[data_dup['rework'] > 2].reset_index(drop=True)['data_id'].unique()

    df_reject = df[df['data_id'].isin(reject_data)].reset_index(drop=True)
    df_reject = df_reject[df_reject['prog_state_cd'].isin(['WORK_END', 'CHECK_REWORK'])].reset_index(drop=True)

    error_data = df_reject[df_reject['result_json'].isnull() == True]['data_id'].unique()
    df_reject = df_reject[~df_reject['data_id'].isin(error_data)].reset_index(drop=True)

    data_list = df_reject['data_id'].unique()

    # Set iou threshold
    iou_thresh = 0.7

    auto_rejects = []
    for data_id in data_list:
        # print("Data ID : ", data_id)
        split_df = df_reject[df_reject['data_id'] == data_id].reset_index(drop=True)

        # Get sequence bboxes list
        seq_json = sequence_bboxes(split_df)
        
        recalls = []
        precisions = []
        for j in range(len(seq_json)-1):
            TP, FP, FN = 0, 0, 0
            # Number of bboxes
            numboxA = len(seq_json[j])
            numboxB = len(seq_json[j+1])
            print(numboxA, numboxB)
            # Bboxes coord
            bboxesA = seq_json[j]
            bboxesB = seq_json[j+1]
            
            if (numboxA > 0) and (numboxB > 0):
                IoUs = []
                for bboxA in bboxesA:
                    iou_list = []
                    for bboxB in bboxesB:
                        iou_list.append(bbox_iou(bboxA, bboxB))
                    
                    iou_max = max(iou_list)
                    IoUs.append(iou_max)

                    max_iou_id = iou_list.index(iou_max)
                    
                    # Compare bbox
                    bboxB = bboxesB[max_iou_id]
                    
                    # Detection True ?
                    if iou_max > iou_thresh:
                        if bboxA[4] == bboxB[4]: # Classification True ?
                            TP += 1
                        else: # Classification False
                            FN += 1
                    else: # Detection False
                        FN += 1

                ############## Confusion Matrix
                # increase bbox
                if numboxB >= numboxA: # Negative detection
                    FP = FP + (numboxB - numboxA)

                # decrease bbox
                else:
                    FN = numboxA - numboxB # Detection False

                # print("=== Confusion Matrix ===")
                # print(TP, FN)
                # print(FP, 'X')
                # print()

                # Calc Recall Precision
                if (TP == 0) and (FP == 0):
                    precision = 0. 
                    recall =  TP / (TP + FN)
                else:        
                    recall = TP / (TP + FN)
                    precision = TP / (TP + FP)
                RP = (round(recall,4), round(precision,4))
                avg_iou = sum(IoUs) / len(IoUs)
                # print('Recall, Precision = {}\tNum Object {} => {}\tAvgIoU = {}'.format(
                #     RP, numboxA, numboxB, avg_iou))
                if avg_iou == 1.:
                    if RP == (1.,1.):
                        if (numboxA / numboxB) == 1.:
                            auto_rejects.append(data_id)
            else:
                break

    print()
    print("Unique data : ", len(df['data_id'].unique()))
    print("Reject Data : ", len(data_list))
    print("Aubse data : ", len(auto_rejects))

    # abuse_df = df[df['data_id'].isin(auto_rejects)].reset_index(drop=True)
    # abuse_df.to_csv('./table/prj_idx_6950_abuse.csv', index=False)


if __name__ == '__main__':
    main()