import pymysql
import pandas as pd
import time
from datetime import datetime


def main():
    info = pymysql.connect(
        user='bigjh64',
        passwd='bigjh64$31',
        host='db-1ea6m.pub-cdb.ntruss.com', # 운영db
        # host='db-svmk.pub-cdb.ntruss.com' # dev db
        port=3306,
        db='cwaidata',
        charset='utf8'
    )

    cursor = info.cursor(pymysql.cursors.DictCursor)

    # # CW_DATA_HIST
    # sql = "SELECT data_id, seq, prog_state_cd, worker_id, work_sdate, work_edate, checker_id, check_sdate, check_edate, work_object_number, result_json, reject_reason \
    #         FROM `CW_DATA_HIST` WHERE data_id > {} AND data_id < {};".format(min_data, max_data)

    sql = "SELECT data_id, seq, prog_state_cd, worker_id, work_sdate, work_edate, checker_id, check_sdate, check_edate, work_object_number, result_json, reject_reason \
            FROM `CW_DATA_HIST` WHERE data_id IN \
                (SELECT data_idx FROM `TB_PRJ_DATA` WHERE prj_idx IN (5616));"

    # # TB_PRJ_DATA
    # sql = "explain SELECT data_idx, prj_idx, project_id, invalid_yn, problem_yn, work_object_number, reject_reason, result_json,\
    #             work_user, work_sdate, work_edate, work_point_type_cd, work_point, check_user, check_sdate, check_edate, \
    #             check_point_type_cd, check_point, platform, hold_yn\
    #             FROM `TB_PRJ_DATA` WHERE prj_idx IN (SELECT prj_idx FROM TB_PRJ_MST WHERE project_id = 1909);"

    """
    explain SELECT data_idx, prj_idx, project_id, invalid_yn, problem_yn, work_object_number, reject_reason, result_json,
                work_user, work_sdate, work_edate, work_point_type_cd, work_point, check_user, check_sdate, check_edate, 
                check_point_type_cd, check_point, platform, hold_yn
                FROM `TB_PRJ_DATA` WHERE prj_idx IN (SELECT prj_idx FROM TB_PRJ_MST WHERE project_id = 1909)
    """

    cursor.execute(sql)
    reuslt=cursor.fetchall()

    df = pd.DataFrame(reuslt)
    print(df)
    print(df.shape)
    df.to_csv('./table/prj_pig_hist.csv', index=False)


if __name__ == '__main__':
    main()
