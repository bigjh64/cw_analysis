import pymysql
import pandas as pd
import time
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import seaborn as sns


def get_db(sql):
    info = pymysql.connect(
        user='bigjh64',
        passwd='bigjh64$31',
        host='db-1ea6m.pub-cdb.ntruss.com', # 운영db
        # host='db-svmk.pub-cdb.ntruss.com' # dev db
        port=3306,
        db='cwaidata',
        charset='utf8'
    )

    cursor = info.cursor(pymysql.cursors.DictCursor)
    cursor.execute(sql)
    reuslt=cursor.fetchall()

    df = pd.DataFrame(reuslt)
    

    return df


def new_member(start_date, end_date):
    """
    신규 가입자 통계
    - 전체 로그인 회원 --> tb_login_hist (??)
    - 신규 가입자 --> tb_member
    - 전체 작업자 --> tb_prj_data
    - 전체 작업수 --> cw_project
    - 탈퇴자 --> tb_member
    """
    # Start ~ End Date
    sdate = datetime.strptime(start_date, '%Y-%m-%d')
    edate = datetime.strptime(end_date, '%Y-%m-%d')

    # TB_MEMBER
    sql = "SELECT member_id, login_id, reg_date, gender_cd, birthday last_login_date \
            FROM `TB_MEMBER` WHERE `reg_date` > '{}' AND `reg_date` < '{}';".format(sdate, edate)

    # Get Table
    df = get_db(sql)
    df['reg_date'] = pd.to_datetime(df['reg_date'])
    df = df.sort_values(by=['reg_date']).reset_index(drop=True)
    df['reg_date'] = df['reg_date'].dt.strftime('%m-%d') # to day
    print(df)
    
    # Total Member Count
    total_new_member = df.shape[0]
    total_F_member = df[df['gender_cd']=='F'].shape[0]
    total_M_member = df[df['gender_cd']=='M'].shape[0]

    # Drawing
    plt.figure(figsize=(7,7))
    plt.title("Total : {}\nF : {}\nM : {}".format(total_new_member, total_F_member, total_M_member))
    sns.countplot(x='reg_date', data=df, color='green', alpha=0.7, label='T')
    sns.countplot(x='reg_date', hue='gender_cd', data=df)
    plt.xticks(rotation=45)
    plt.xlabel('{} ~ {}'.format(start_date, end_date))
    plt.ylabel('Count')
    plt.legend()
    plt.show()


def active_member(start_date, end_date):
    """
    활동 회원 통계
    - 실제로 작업에 참여한 이력이 있는 회원 수 카운팅
    - 하루 적립된 작업자/검수자 포인트 합계
    """

    # Start ~ End Date
    sdate = datetime.strptime(start_date, '%Y-%m-%d')
    edate = datetime.strptime(end_date, '%Y-%m-%d')

    margin_sdate = sdate - timedelta(days=15)

    print('Searching ... {} ~ {}'.format(margin_sdate, edate))

    # SQL
    sql = "SELECT data_idx, prj_idx, project_id, work_user, work_sdate, work_point, check_user, check_sdate, check_point \
            FROM `TB_PRJ_DATA` WHERE prj_idx IN \
                (SELECT prj_idx FROM `TB_PRJ_MST` WHERE prj_sdate >= '{}' AND prj_sdate <= '{}');".format(margin_sdate, edate)

    # Get DB
    df = get_db(sql)
    print("Success Get table")

    # To datetime type
    for d in ['work_sdate', 'check_sdate']:
        df[d] = pd.to_datetime(df[d])



    ################################## Worker
    # Worker pre-processing
    wdf = df[df['work_sdate'] >= sdate].reset_index(drop=True)
    wdf = wdf[wdf['work_sdate'] < edate].reset_index(drop=True)

    # Sort by 'work_sdate'
    wdf = wdf.sort_values(by=['work_sdate']).reset_index(drop=True)
    
    # To month-day
    for d in ['work_sdate', 'check_sdate']:
        wdf[d] = wdf[d].dt.strftime('%m-%d')

    # Daily Active Member
    daily_wdf = wdf.drop_duplicates(['work_user', 'work_sdate'], keep='first').reset_index(drop=True)

    # Daily Work Point
    daily_wpoint = wdf.groupby(['work_sdate'], as_index=True).sum()['work_point'].reset_index(drop=False)



    ################################## Checker
    # Checker pre-processing
    cdf = df[df['check_sdate'] >= sdate].reset_index(drop=True)
    cdf = cdf[cdf['check_sdate'] < edate].reset_index(drop=True)

    # Sort by 'check_sdate'
    cdf = cdf.sort_values(by=['check_sdate']).reset_index(drop=True)
    
    # To month-day
    for d in ['work_sdate', 'check_sdate']:
        cdf[d] = cdf[d].dt.strftime('%m-%d')

    # Daily Active Member
    daily_cdf = cdf.drop_duplicates(['check_user', 'check_sdate'], keep='first').reset_index(drop=True)

    # Daily Check Point
    daily_cpoint = cdf.groupby(['check_sdate'], as_index=True).sum()['check_point'].reset_index(drop=False)


    ################################## Visualization

    # Drawing DAU
    plt.figure(figsize=(5,5))
    plt.title('DAU')
    sns.countplot(x='work_sdate', data=daily_wdf, color='red', alpha=0.7, label='worker')
    sns.countplot(x='check_sdate', data=daily_cdf, color='blue', alpha=0.7, label='checker')
    plt.xticks(rotation=45)
    plt.xlabel('{} ~ {}'.format(start_date, end_date))
    plt.ylabel('Count')
    plt.legend()
    # plt.show()
    
    # Drawing Daily Points
    plt.figure(figsize=(5,5))
    plt.title("Daily Point")
    plt.bar(
        daily_wpoint['work_sdate'], 
        daily_wpoint['work_point'],
        color='red',
        alpha=0.8,
        label='Worker')

    plt.bar(
        daily_cpoint['check_sdate'], 
        daily_cpoint['check_point'],
        color='blue',
        alpha=0.8,
        label='Checker')
    plt.xlabel("{} ~ {}".format(start_date, end_date))
    plt.ylabel("Point")
    plt.xticks(rotation=45)
    plt.legend()
    plt.show()

    
if __name__ == '__main__':
    active_member('2020-02-24', '2020-03-02')
    new_member('2020-02-24', '2020-03-02')
