import pymysql
import pandas as pd
import time
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

def get_db(sql):
    info = pymysql.connect(
        user='bigjh64',
        passwd='bigjh64$31',
        host='db-1ea6m.pub-cdb.ntruss.com', # 운영db
        # host='db-svmk.pub-cdb.ntruss.com' # dev db
        port=3306,
        db='cwaidata',
        charset='utf8'
    )

    cursor = info.cursor(pymysql.cursors.DictCursor)
    cursor.execute(sql)
    reuslt=cursor.fetchall()

    df = pd.DataFrame(reuslt)
    df.to_csv('./table/prj2626_hist.csv', index=False)
    
    return df


def main():
    # # EN OCR Project Hist Data
    # sql = "SELECT data_id, seq, prog_state_cd, worker_id, work_sdate, work_edate, checker_id, check_sdate, check_edate, work_object_number \
    #         FROM `CW_DATA_HIST` WHERE data_id IN \
    #             (SELECT data_idx FROM `TB_PRJ_DATA` WHERE prj_idx IN (6128,6129,6130,6131,6132,6133))"

    # df = get_db(sql)

    # Read table
    df = pd.read_csv("./table/prj2626_hist.csv")
    print('EN OCR project DF shape ', df.shape)

    # Get first work/check data id
    df = df[df['seq'] == 2].reset_index(drop=True)
    print('First check data: ', df.shape)

    # Get check success data id
    df = df[df['prog_state_cd'] == 'CHECK_END']
    print('Check success data: ', df.shape)

    # To datetime
    for d in ['work_sdate', 'work_edate', 'check_sdate', 'check_edate']:
        df[d] = pd.to_datetime(df[d])

    # Generate feature
    df['work_dur'] = (df['work_edate'] - df['work_sdate']).dt.total_seconds() # work duration
    df['check_dur'] = (df['check_edate'] - df['check_sdate']).dt.total_seconds() # check_duration
    df['check_resp'] = (df['check_sdate'] - df['work_edate']).dt.total_seconds() # check_response

    tmp_dict = {}
    for k in df.keys():
        tmp_dict[k] = [0]

    zero_df = pd.DataFrame(tmp_dict)
    
    for i, checker in enumerate(df['checker_id'].unique()):
        checker_df = df[df['checker_id'] == checker].reset_index(drop=True)
        checker_df = checker_df.sort_values(by=['check_sdate']).reset_index(drop=True)
        upper_df = checker_df.append(zero_df).reset_index(drop=True)
        upper_df = upper_df.drop(0, 0).reset_index(drop=True)
        upper_df['check_sdate'][-1:] = checker_df['check_edate'][-1:]

        check_check = (pd.to_datetime(upper_df['check_sdate']) - pd.to_datetime(checker_df['check_edate'])).dt.total_seconds().to_list()
        check_check.insert(0, 0)
        del check_check[-1]

        checker_df['check_check'] = check_check
        print((checker_df['check_check'] < 0).sum())
    

if __name__ == '__main__':
    main()